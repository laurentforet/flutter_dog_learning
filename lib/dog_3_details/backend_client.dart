import 'dart:convert';
import 'dart:async';
import 'package:flutter_dog_learning/dog_3_details/model.dart';
import 'package:http/http.dart' as http;


Future<List<Dog>> fetchDogs() async {
  final response = await http.get('http://10.0.2.2:8080/dog?nb=12');
  var decodes = json.decode(response.body);
  return Stream.fromIterable(decodes)
      .map((decode) => Dog.fromJson(decode))
      .toList();
}