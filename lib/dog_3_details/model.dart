class Dog {
  String url;
  String name;
  String description;

  Dog({this.url, this.name, this.description});

  factory Dog.fromJson(Map<String, dynamic> json) {
    return Dog(
      url: json['url'],
      name: json['name'],
      description: json['description'],
    );
  }
static final List<Dog> DOGS = [Dog(name: 'toto'), Dog(name: 'titi'), Dog(name: 'tutu')];
}


