import 'dart:collection';
import 'dart:math';
import 'dart:ui';

class Product {
  final int id;
  final String name;
  final Color color;

  const Product(this.id, this.name, this.color);

  bool isDark() {
    final luminence = (0.2126 * color.red + 0.7152 * color.green + 0.0722 * color.blue);
    return luminence < 150;
  }
  
  @override
  int get hashCode => id;

  @override
  bool operator ==(other) => other is Product && other.hashCode == hashCode;

  @override
  String toString() => "$name (id=$id)";
}

class CartItem {
  final int count;
  final Product product;
  
  const CartItem(this.count, this.product);

  
  
  @override
  String toString() => "${product.name} ✕ $count";
}

class Cart {
  final List<CartItem> _items = <CartItem>[];

  Cart();

  Cart.clone(Cart cart) {
    _items.addAll(cart._items);
  }

  Cart.sample(Iterable<Product> products) {
    _items.addAll(products.take(3).map((product) => CartItem(1, product)));
  }

  int get itemCount => _items.fold(0, (sum, el) => sum + el.count);

  UnmodifiableListView<CartItem> get items => UnmodifiableListView(_items);

  void add(Product product, [int count = 1]) {
    _updateCount(product, count);
  }

  void remove(Product product, [int count = 1]) {
    _updateCount(product, -count);
  }

  @override
  String toString() => "$items";

  void _updateCount(Product product, int difference) {
    if (difference == 0) return;
    for (int i = 0; i < _items.length; i++) {
      final item = _items[i];
      if (product == item.product) {
        final newCount = item.count + difference;
        if (newCount <= 0) {
          _items.removeAt(i);
          return;
        }
        _items[i] = CartItem(newCount, item.product);
        return;
      }
    }
    if (difference < 0) return;
    _items.add(CartItem(max(difference, 0), product));
  }
}


final Catalog catalog = fetchCatalogSync();

/// Fetches the catalog of products asynchronously.
Future<Catalog> fetchCatalog() {
  // This simulates a short delay so that we don't get too cocky about having
  // this state present from application start (something unlikely to happen
  // in the real world).
  return Future.delayed(const Duration(milliseconds: 200), fetchCatalogSync);
}

/// Fetches the catalog synchronously.
///
/// This is much less realistic than [fetchCatalog] but acceptable if we want
/// to focus on some other aspect with our sample.
Catalog fetchCatalogSync() {
  return Catalog._sample();
}

/// Updates an existing [catalog] of products asynchronously.
Future<Null> updateCatalog(Catalog catalog) {
  return Future.delayed(const Duration(milliseconds: 200), () {
    catalog._products.clear();
    catalog._products.addAll(Catalog._sampleProducts);
  });
}

class Catalog {
  /// A listing of sample products.
  static const List<Product> _sampleProducts = const <Product>[
    const Product(42, "Sweater", const Color(0xFF536DFE)),
    const Product(1024, "Socks", const Color(0xFFFFD500)),
    const Product(1337, "Shawl", const Color(0xFF1CE8B5)),
    const Product(123, "Jacket", const Color(0xFFFF6C00)),
    const Product(201805, "Hat", const Color(0xFF574DDD)),
    const Product(128, "Hoodie", const Color(0xFFABD0F2)),
    const Product(321, "Tuxedo", const Color(0xFF8DA0FC)),
    const Product(1003, "Shirt", const Color(0xFF1CE8B5)),
  ];
  
  final List<Product> _products;
  
  Catalog.empty() : _products = [];
  
  Catalog._sample() : _products = _sampleProducts;
  
  bool get isEmpty => _products.isEmpty;
  
  /// An immutable listing of the products.
  UnmodifiableListView<Product> get products =>
      UnmodifiableListView<Product>(_products);
}
