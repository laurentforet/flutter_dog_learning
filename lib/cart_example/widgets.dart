import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:flutter_dog_learning/cart_example/models.dart';

class ProductSquare extends StatelessWidget {
   final Product product;
   
   final GestureTapCallback onTap;
   
   ProductSquare({
      Key key,
      @required this.product,
      this.onTap,
   }) : super(key: key);
   
   @override
   Widget build(BuildContext context) {
      return Material(
         color: product.color,
         child: InkWell(
            onTap: onTap,
            child: Center(
                child: Text(
                   product.name,
                   style: TextStyle(
                       color: product.isDark() ? Colors.white : Colors.black),
                )),
         ),
      );
   }
}


class CartButton extends StatefulWidget {
   final VoidCallback onPressed;
   final int itemCount;
   final Color badgeColor;
   final Color badgeTextColor;
   
   CartButton({
      Key key,
      @required this.itemCount,
      this.onPressed,
      this.badgeColor: Colors.red,
      this.badgeTextColor: Colors.white,
   })  : assert(itemCount >= 0),
          assert(badgeColor != null),
          assert(badgeTextColor != null),
          super(key: key);
   
   @override
   CartButtonState createState() {
      return CartButtonState();
   }
}

class CartButtonState extends State<CartButton>
    with SingleTickerProviderStateMixin {
   AnimationController _animationController;
   Animation<double> _animation;
   
   final Tween<Offset> _badgePositionTween = Tween(
      begin: const Offset(-0.5, 0.9),
      end: const Offset(0.0, 0.0),
   );
   
   @override
   Widget build(BuildContext context) {
      return IconButton(
          icon: Stack(
             overflow: Overflow.visible,
             children: [
                Icon(Icons.shopping_cart),
                Positioned(
                   top: -8.0,
                   right: -3.0,
                   child: SlideTransition(
                      position: _badgePositionTween.animate(_animation),
                      child: Material(
                          type: MaterialType.circle,
                          elevation: 2.0,
                          color: Colors.red,
                          child: Padding(
                             padding: const EdgeInsets.all(5.0),
                             child: Text(
                                widget.itemCount.toString(),
                                style: TextStyle(
                                   fontSize: 13.0,
                                   color: widget.badgeTextColor,
                                   fontWeight: FontWeight.bold,
                                ),
                             ),
                          )),
                   ),
                ),
             ],
          ),
          onPressed: widget.onPressed);
   }
   
   @override
   void didUpdateWidget(CartButton oldWidget) {
      if (widget.itemCount != oldWidget.itemCount) {
         _animationController.reset();
         _animationController.forward();
      }
      super.didUpdateWidget(oldWidget);
   }
   
   @override
   void dispose() {
      _animationController.dispose();
      super.dispose();
   }
   
   @override
   void initState() {
      super.initState();
      _animationController = AnimationController(
         duration: const Duration(milliseconds: 500),
         vsync: this,
      );
      _animation =
          CurvedAnimation(parent: _animationController, curve: Curves.elasticOut);
      _animationController.forward();
   }
}




class CartPage extends StatelessWidget {
   CartPage(this.cart);
   final Cart cart;
   
   static const routeName = "/cart";
   
   @override
   Widget build(BuildContext context) {
      return Scaffold(
         appBar: AppBar(
            title: Text("Your Cart"),
         ),
         body: cart.items.isEmpty
             ? Center(
             child: Text('Empty', style: Theme.of(context).textTheme.display1))
             : ListView(
             children:
             cart.items.map((item) => ItemTile(item: item)).toList()),
      );
   }
}

class ItemTile extends StatelessWidget {
   ItemTile({this.item});
   final CartItem item;
   
   @override
   Widget build(BuildContext context) {
      final textStyle = TextStyle(
          color: item.product.isDark() ? Colors.white : Colors.black);
      
      return Container(
         color: item.product.color,
         child: ListTile(
            title: Text(
               item.product.name,
               style: textStyle,
            ),
            trailing: CircleAvatar(
                backgroundColor: const Color(0x33FFFFFF),
                child: Text(item.count.toString(), style: textStyle)),
         ),
      );
   }
}
