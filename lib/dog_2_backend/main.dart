import 'package:flutter/material.dart';
import 'package:flutter_dog_learning/dog_2_backend/backend_client.dart';

const int _bdxJugPrimaryValue = 0xFFE21F1F;
const MaterialColor bdxRed = MaterialColor(
  _bdxJugPrimaryValue,
  <int, Color>{
    50: Color(0xFFFFEBEE),
    100: Color(0xFFFFCDD2),
    200: Color(0xFFEF9A9A),
    300: Color(0xFFE57373),
    400: Color(0xFFEF5350),
    500: Color(_bdxJugPrimaryValue),
    600: Color(0xFFE53935),
    700: Color(0xFFD32F2F),
    800: Color(0xFFC62828),
    900: Color(0xFFB71C1C),
  },
);

void main() {
  runApp(DogApp());
}

class DogApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'BordeauxJUG Demo',
        theme: ThemeData(primarySwatch: bdxRed),
        home: DogHomePage());
  }
}

class DogHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Super BdxJug Dog App"),
      ),
      body: DogList(),
    );
  }
}

class DogList extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: fetchDogs(),
      builder: (context, snapshot) {
        return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext ctx, int index) {
              if (!snapshot.hasData) {
                return Text('Wait please the fetch');
              }
              var dog = snapshot.data[index];
              return ListTile(
                title: Text(dog.name,
                  style: TextStyle(fontSize: 24),),
                leading:  CircleAvatar(
                  child: Text(''),
                  backgroundImage: NetworkImage(dog.url),
                ),
              );
            });
      }
      );
  }
}

