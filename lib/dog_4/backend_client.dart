import 'dart:convert';
import 'dart:async';
import 'package:eventsource/eventsource.dart';
import 'package:flutter_dog_learning/dog_4/model.dart';
import 'package:http/http.dart' as http;


Future<List<Dog>> fetchDogs() async {
  final response = await http.get('http://10.0.2.2:8080/dog?nb=12');
  var decodes = json.decode(response.body);
  return Stream.fromIterable(decodes)
      .map((decode) => Dog.fromJson(decode))
      .toList();
}

Future<Stream<Dog>> fetchDogsContinuously() async {
  final EventSource eventSource =
      await EventSource.connect('http://10.0.2.2:8080/dog/events');
  return eventSource.map((event) =>
      Dog.fromJson(json.decode(event.data)));
}