import 'package:flutter_dog_learning/dog_4/backend_client.dart';
import 'package:flutter_dog_learning/dog_4/model.dart';
import 'package:rxdart/rxdart.dart';

class DogBloc {
  List<Dog> _dogs = [];

  final BehaviorSubject<List<Dog>> _dogsBehavior = BehaviorSubject<List<Dog>>(seedValue: []);
  
  DogBloc() {
    fetchDogsContinuously().then((Stream<Dog> dogStream) {
      dogStream.listen((Dog data) {
        _dogs.add(data);
        _dogsBehavior.add(_dogs);
      });
    });
  }
  
  Stream<List<Dog>> get dogs => _dogsBehavior.stream;
  
}
