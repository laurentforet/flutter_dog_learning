import 'package:flutter_dog_learning/dog/backend_client.dart';
import 'package:flutter_dog_learning/dog/models.dart';
import 'package:flutter_dog_learning/dog/business_logic.dart';
import 'package:flutter/material.dart';

const int _bdxJugPrimaryValue = 0xFFE21F1F;
const MaterialColor bdxRed = MaterialColor(
  _bdxJugPrimaryValue,
  <int, Color>{
    50: Color(0xFFFFEBEE),
    100: Color(0xFFFFCDD2),
    200: Color(0xFFEF9A9A),
    300: Color(0xFFE57373),
    400: Color(0xFFEF5350),
    500: Color(_bdxJugPrimaryValue),
    600: Color(0xFFE53935),
    700: Color(0xFFD32F2F),
    800: Color(0xFFC62828),
    900: Color(0xFFB71C1C),
  },
);

void main() => runApp(DogApp());

class DogApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DogProvider(
      child: MaterialApp(
        title: "Dog shop",
        theme: ThemeData(
          primarySwatch: bdxRed,
        ),
        home: DogHomePage(),
//         initialRoute: '/',
//         routes: {
//            '/': (context) => DogHomePage(),
//            '/detail': (context) => DogDetailPage()
//         }
      ),
    );
  }
}

class DogHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BordeauxJug Dog Shop'),
      ),
      body: DogList(),
    );
  }
}

class DogProvider extends InheritedWidget {
  
  final DogBloc dogBloc;
  
  DogProvider({
    Key key,
    DogBloc dogBloc,
    Widget child,
  })  : dogBloc = dogBloc ?? DogBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static DogBloc of(BuildContext context) =>
      (context.inheritFromWidgetOfExactType(DogProvider) as DogProvider)
          .dogBloc;
}

class DogList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var dogProvider = DogProvider.of(context);
    return StreamBuilder<List<Dog>>(
        stream: dogProvider.dogs,
        builder: (BuildContext context, AsyncSnapshot<List<Dog>> snapshot) {
          if (!snapshot.hasData) {
            return Text('no Data');
          } else {
            var dogsToDisplay = snapshot.data;
            return ListView.builder(
                itemCount: dogsToDisplay.length,
                itemBuilder: (BuildContext context, int index) {
                  Dog dog = dogsToDisplay[index];
                  return Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: ListTile(
                      onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DogDetailPage(dog))),
//                      onTap: Navigator.of(context).pushNamed(DogDetailPage.routeName);,
                    
                      title: Text(
                        dog.name,
                        style: TextStyle(fontSize: 18.0),
                      ),
                      leading: CircleAvatar(
                        child: Text(''),
                        backgroundImage: NetworkImage(dog.url),
                      ),
                    ),
                  );
                });
          }
        });
  }

  Future<List<Dog>> _getDogs() async {
    return fetchDogs();
  }
}

class DogDetailPage extends StatelessWidget {
  
  static const routeName = "/detail";
  
  final Dog dog;

  DogDetailPage(this.dog);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(dog.name),
      ),
      body: ListView(
          children: <Widget>[
            Flex(
              direction: Axis.vertical,
              children: <Widget>[
                Image.network(dog.url)
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(dog.description),
            ),
          ],
        ),
      floatingActionButton: FloatingActionButton(
        onPressed: _doSomething,
        tooltip: 'Adoptez-moi',
        child: Icon(Icons.send),
      ),
    );
  }
  
  void _doSomething() {
  }
}
