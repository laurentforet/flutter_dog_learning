import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:eventsource/eventsource.dart';

import 'package:flutter_dog_learning/dog/models.dart';

Future<List<Dog>> fetchDogs() async {
  // AppData\Local\Android\sdk\emulator\emulator.exe -avd Nexus_5X_API_26 -dns-server 8.8.8.8
  final response = await http.get('http://10.0.2.2:8080/dog?nb=12');
  var decodes = json.decode(response.body);
  return Stream.fromIterable(decodes)
      .map((decode) => Dog.fromJson(decode))
      .toList();
}

Future<Stream<Dog>> fetchDogsContinuously() async {
  final EventSource eventSource =
      await EventSource.connect('http://10.0.2.2:8080/dog/events');
  return eventSource.map((event) => Dog.fromJson(json.decode(event.data)));
}
