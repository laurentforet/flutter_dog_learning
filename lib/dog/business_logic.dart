import 'dart:async';
import 'package:rxdart/subjects.dart';
import 'package:flutter_dog_learning/dog/models.dart';
import 'package:flutter_dog_learning/dog/backend_client.dart';

class DogBloc {
   
   final List<Dog> _dogs = [];
   final BehaviorSubject<List<Dog>> _dogsBehavior = BehaviorSubject<List<Dog>>(seedValue: []);
   
   DogBloc() {
      fetchDogsContinuously().then((dogStream) {
         dogStream.listen((dog) {
            _dogs.add(dog);
            _dogsBehavior.add(_dogs);
         });
      });
   }
   Stream<List<Dog>> get dogs => _dogsBehavior.stream;
}