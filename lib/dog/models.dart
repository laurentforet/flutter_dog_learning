

class Dog {
   String url;
   String name;
   String description;

   Dog({this.url, this.name, this.description});

   factory Dog.fromJson(Map<String, dynamic> json) {
      return Dog(
         url: json['url'],
         name: json['name'],
         description: json['description'],
      );
   }

}
Dog toto = new Dog(name: 'toto', url: 'http://toto.com');
Dog titi = new Dog(name: 'titi', url: 'http://titi.com');
Dog tutu = new Dog(name: 'tutu', url: 'http://tutu.com');
final List<Dog> DOGS = [toto, titi, tutu];
